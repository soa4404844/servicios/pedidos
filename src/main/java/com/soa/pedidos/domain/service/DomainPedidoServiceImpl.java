package com.soa.pedidos.domain.service;

import com.soa.pedidos.domain.model.Pedido;
import com.soa.pedidos.domain.repository.DomainPedidoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DomainPedidoServiceImpl implements DomainPedidoService {

    @Autowired
    private DomainPedidoRepository domainPedidoRepository;

    @Override
    public Pedido savePedido(Pedido pedido) {
        return domainPedidoRepository.save(pedido);
    }

    @Override
    public List<Pedido> getPedidos(String parametro, Pedido parametros) {
        return domainPedidoRepository.findByPorParametro(parametro, parametros);
    }

    @Override
    public Pedido getPedido(Pedido pedido) {
        return domainPedidoRepository.findById(pedido.getPedidoID())
                .orElse(Pedido.builder().build());
    }
}
