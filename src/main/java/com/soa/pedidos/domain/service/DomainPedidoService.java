package com.soa.pedidos.domain.service;

import com.soa.pedidos.domain.model.Pedido;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DomainPedidoService {

    Pedido savePedido(Pedido pedido);

    List<Pedido> getPedidos(String parametro, Pedido parametros);

    Pedido getPedido(Pedido pedido);


}
