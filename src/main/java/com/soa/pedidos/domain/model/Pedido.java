package com.soa.pedidos.domain.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDate;
import java.time.OffsetDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "PEDIDO")
public class Pedido {


    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "pedido_id")
    private Integer pedidoID;

    @Column(name = "venta_id")
    private Integer ventaID;

    @Column(name = "contacto_id")
    private Integer contactoID;

    @Column(name = "operador_id")
    private Integer operadorID;

    @Column(name = "estado")
    private String estado;

    @Column(name = "fecha_entrega")
    private LocalDate fechaEntrega;

    @Column(name = "observaciones")
    private String observaciones;

    @CreationTimestamp
    @Column(name = "fecha_transaccion")
    private OffsetDateTime fechaTransaccion;

}