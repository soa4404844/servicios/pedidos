package com.soa.pedidos.domain.repository;

import com.soa.pedidos.domain.model.Pedido;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface DomainPedidoRepository extends CrudRepository<Pedido, Integer> {

    @Query( value = """
            SELECT P.* FROM PEDIDO P
            LEFT JOIN CONTACTO C ON C.contacto_id = P.contacto_id
            WHERE (:#{#pedidoTabla.pedidoID} IS NULL OR P.pedido_id = :#{#pedidoTabla.pedidoID})
            AND (:#{#pedidoTabla.estado} IS NULL OR P.estado = :#{#pedidoTabla.estado})
            AND (:parametro IS NULL
                OR C.nombre LIKE %:parametro%
                OR C.apellidos LIKE %:parametro%
                OR C.numero_documento LIKE %:parametro%
            )
            ORDER BY P.fecha_transaccion ASC
            """,
    nativeQuery = true)
    List<Pedido> findByPorParametro(
            @Param("parametro") String parametro,
            @Param("pedidoTabla") Pedido pedido
    );

}
