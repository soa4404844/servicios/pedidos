package com.soa.pedidos.application.mapper;

import com.soa.pedidos.application.model.GuardarPedido;
import com.soa.pedidos.application.model.ObtenerPedido;
import com.soa.pedidos.domain.model.Pedido;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ApplicationPedidoMapper {

    Pedido guardarPedidoToPedido(GuardarPedido guardarPedido);

    GuardarPedido pedidoToGuardarPedido(Pedido pedido);

    Pedido obtenerPedidoToPedido(ObtenerPedido obtenerPedido);

    ObtenerPedido pedidoToObtenerPedido(Pedido pedido);



}
