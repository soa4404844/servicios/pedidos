package com.soa.pedidos.application.service;


import com.soa.pedidos.application.api.PedidoApiDelegate;
import com.soa.pedidos.application.mapper.ApplicationPedidoMapper;
import com.soa.pedidos.application.model.GuardarPedido;
import com.soa.pedidos.application.model.ObtenerPedido;
import com.soa.pedidos.domain.model.Pedido;
import com.soa.pedidos.domain.service.DomainPedidoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class ApplicationPedidoServiceImpl implements PedidoApiDelegate {

    @Autowired
    private ApplicationPedidoMapper applicationPedidoMapper;

    @Autowired
    private DomainPedidoService domainPedidoService;

    @Override
    public ResponseEntity<GuardarPedido> guardarPedido(GuardarPedido guardarPedido) {

        var pedido = domainPedidoService.savePedido(
                applicationPedidoMapper.guardarPedidoToPedido(
                        guardarPedido
                )
        );
        return ResponseEntity.ok(
                applicationPedidoMapper.pedidoToGuardarPedido(pedido)
        );
    }

    @Override
    public ResponseEntity<List<ObtenerPedido>> consultarPedido(String parametro, ObtenerPedido parametros) {
        var pedidos = domainPedidoService.getPedidos(
                parametro,
                applicationPedidoMapper.obtenerPedidoToPedido(parametros)
        );

        return ResponseEntity.ok(
                pedidos.stream()
                        .map(applicationPedidoMapper::pedidoToObtenerPedido)
                        .toList()
        );
    }

    @Override
    public ResponseEntity<ObtenerPedido> obtenerPedido(Integer pedidoId) {

        var pedido = domainPedidoService.getPedido(Pedido.builder()
                .pedidoID(pedidoId)
                .build()
        );

        return ResponseEntity.ok(
                applicationPedidoMapper.pedidoToObtenerPedido(pedido)
        );
    }


}

